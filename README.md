# README #

This document is for understanding of development task app.


### Technology stack used for this app ###

* Xcode 11.6
* Swift 5.0
* Architecture pattern : VIPER


### Dependencies ###

* 'GoogleMaps'
* 'GooglePlaces'
* 'GoogleSignIn'
* 'FBSDKLoginKit'
* 'Firebase/Analytics'
* 'Firebase/Crashlytics'
* 'Firebase/Auth'
* 'Google-Mobile-Ads-SDK'

### Functionalities ###

* Login With social media: Facebook, Google
* Fetch users current location and display near by restaurants within 5 miles.
* Show near by restaurants in list view format with details of that restaurants (Name,Rating,Address)
* Fetch restaurants using Zipcode.
* Google AdMob banner ad at the bottom the map screen.
* Containg firebase analytics to track users activity.
* Crashlytics to track crash logs.
