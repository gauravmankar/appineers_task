//
//  Utilities.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps

class Utilities {
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    
    static var sharedInstance = Utilities()
    
    private init() {
        
    }
    
    // Shows alert view.
    func generateAlertViewWithTitle(title: String, message: String, sender: UIViewController) -> Void{
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        sender.present(alert, animated: true, completion: nil)
    }
    
    
}
