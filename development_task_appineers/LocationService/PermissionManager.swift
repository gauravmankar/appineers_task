//
//  PermissionManager.swift
//  PermissionManager


import UIKit
import CoreLocation
import Contacts
import UserNotifications

public protocol PermissionManagerDelegate: class {

    func requirePermission(_ aPermissionType: PermissionType)
}

public protocol LocationDelegate: PermissionManagerDelegate {
    
    func didUpdateLocations(_ locations:[CLLocation], _ manager: CLLocationManager)
    func didFailWithError(_ error:Error, _ manager: CLLocationManager)
    func didDetermineState(_ state: CLRegionState, _region: CLRegion, _ manager: CLLocationManager)    
    func didChangeAuthorizationStatus(_ status: CLAuthorizationStatus, _ manager: CLLocationManager)
}

public enum PermissionType {
    case permissionTypeLocation
}

class PermissionManager: NSObject {

    static let sharedInstance = PermissionManager()

    weak var locationDelegate: LocationDelegate?
    
    var locationManager = CLLocationManager()
    var lat: String = "0"
    var long: String = "0"

    func setPermissionType(permission: PermissionType) {
        switch permission {
        case .permissionTypeLocation:
            initLocation()
        default:
            break
        }
    }
}
