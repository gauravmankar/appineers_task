//
//  Restaurants.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation

struct Restaurants: Codable {
    
    struct Results: Codable {
        var name: String
        var rating: Float
        var location: String
        
        enum codingKeys: String, CodingKey {
            case name
            case rating
            case location = "vicinity"
        }
    }
    var results: [Results]
}
