//
//  RestaurantsTableViewDataSource.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

final class RestaurantsTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    var restaurantData: [Dictionary<String, AnyObject>] = Array()
    
    var router: GetPlacesRouter
    
    init(items: [Dictionary<String, AnyObject>], router: GetPlacesRouter) {
        self.router = router
        self.restaurantData = items
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantsTableViewCell", for: indexPath) as! RestaurantsTableViewCell
        
        let restaurant = restaurantData[indexPath.row]
        
        cell.restaurantNameLabel.text = restaurant["name"] as? String
        cell.ratingButton.setTitle("\(String(describing: restaurant["rating"]!))", for: .normal)
        
        if let address = restaurant["vicinity"] as? String {
            cell.locationLabel.text = address
        }else{
            cell.locationLabel.text = restaurant["formatted_address"] as? String
        }
        
        return cell
    }
    
    
}
