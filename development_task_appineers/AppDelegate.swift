//
//  AppDelegate.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 29/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import FBSDKCoreKit
import Firebase
import GoogleMobileAds
import FirebaseAuth

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var searchResults: [Dictionary<String, AnyObject>] = Array()
    
    var locationManager = CLLocationManager()
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        
        // Firebase analytics
        FirebaseApp.configure()
        
        // Google admob ads
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = Constants.GOOGLE_CLIENT_ID
        
        //Initilizing Google map services API Key
        GMSServices.provideAPIKey(Constants.GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(Constants.GOOGLE_CLIENT_ID)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "development_tast_appineers")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate: LocationDelegate{
    
    func requirePermission(_ aPermissionType: PermissionType) {
        print("required permission")
      //  self.locationManager.requestLocation()
        
    }
    
    func didUpdateLocations(_ locations: [CLLocation], _ manager: CLLocationManager) {
        self.locationManager = manager
    }
    
    func didFailWithError(_ error: Error, _ manager: CLLocationManager) {
        
    }
    
    func didDetermineState(_ state: CLRegionState, _region: CLRegion, _ manager: CLLocationManager) {
        
    }
    
    func didChangeAuthorizationStatus(_ status: CLAuthorizationStatus, _ manager: CLLocationManager) {
    }
}

