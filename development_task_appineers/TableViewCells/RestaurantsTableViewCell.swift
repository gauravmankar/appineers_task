//
//  RestaurantsTableViewCell.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var ratingButton: UIButton!
    
    static let identifier = "RestaurantsTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "RestaurantsTableViewCell", bundle: nil)
    }
    
    public func configTableViewCell(with location: Dictionary<String, AnyObject>) {
        restaurantNameLabel.text = location["name"] as? String ?? ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
