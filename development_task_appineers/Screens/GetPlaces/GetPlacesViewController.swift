//
//  GetPlacesViewController.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 29/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GoogleSignIn
import GoogleMobileAds

protocol GetPlacesViewProtocol {
    func reloadRestaurantList()
}

class GetPlacesViewController: UIViewController,GMSMapViewDelegate, GetPlacesViewProtocol {
    
    private var placesClient: GMSPlacesClient!
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    var presenter: GetPlacesPresenter?
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    var tableViewSource: RestaurantsTableViewDataSource?
    var router: GetPlacesRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLocationPermission(self)
        
        router = GetPlacesRouter(rootVC: self)
        presenter = GetPlacesPresenter(appDelegate: appDelegate, view: self, interactor: GetPlacesInteractor())
        self.loadGoogleAd()
        self.getNearByRestaurants()
        placesClient = GMSPlacesClient.shared()
    }
    
    //MARK: - Load Google AdMob Banner Ad
    
    func loadGoogleAd() {
        presenter?.loadGoogleAds(bannerView: bannerView, viewController: self)
    }
    
    //MARK: - Get near by location from current location
    
    func getNearByRestaurants() {
      
        self.getCurrentLocation()
        
        searchBar.searchTextField.addTarget(self, action: #selector(searchPlaceByZipCode), for: .editingChanged)
        tableView.register(RestaurantsTableViewCell.nib(), forCellReuseIdentifier: RestaurantsTableViewCell.identifier)
        tableViewSource = RestaurantsTableViewDataSource(items: Array(), router: self.router!)
        tableView.separatorStyle = .none
        tableView.dataSource = tableViewSource
        tableView.delegate = tableViewSource
    }
    
    func getCurrentLocation(){
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            presenter?.getCurrentLocation(mapView: self.mapView, searchBar: self.searchBar)
        }else{
            presenter?.allowUserToEnableLocationService(viewController: self)
        }
        
    }
    
    @objc func searchPlaceByZipCode() {
        if let searchQuery = searchBar.searchTextField.text {
            if searchQuery.count == 6 {
                presenter?.searchNearByPlacesByZipCode(zipCode: searchQuery, mapView: self.mapView)
            }
        }
    }
    
    //MARK: - Reload restaurant list
    
    func reloadRestaurantList() {
        DispatchQueue.main.async { [self] in
            self.tableViewSource!.restaurantData = self.appDelegate.searchResults
            self.tableView.reloadData()
        }
    }
    
    //MARK: - On change segment control
    
    @IBAction func onChangeViewType(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.view.endEditing(true)
            self.mapView.isHidden = false
            self.tableView.isHidden = true
            self.getNearByRestaurants()
            break
        case 1:
            self.view.endEditing(true)
            self.mapView.isHidden = true
            self.tableView.isHidden = false
            self.getNearByRestaurants()
            break
        default:
            break
        }
    }
    
    //MARK: - Sign out
    
    @IBAction func onTapSignOut(_ sender: Any) {
        presenter?.onSignout()
    }
    
}

extension GetPlacesViewController: LocationDelegate {
    func didUpdateLocations(_ locations: [CLLocation], _ manager: CLLocationManager) {
        
    }
    
    func didFailWithError(_ error: Error, _ manager: CLLocationManager) {
        
    }
    
    func didDetermineState(_ state: CLRegionState, _region: CLRegion, _ manager: CLLocationManager) {
        
    }
    
    func didChangeAuthorizationStatus(_ status: CLAuthorizationStatus, _ manager: CLLocationManager) {
        
    }
    
    func requirePermission(_ aPermissionType: PermissionType) {
        
    }
}
