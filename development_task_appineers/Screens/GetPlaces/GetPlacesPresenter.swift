//
//  GetPlacesPresenter.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps
import GoogleMobileAds

protocol GetPlacesPresenterProtocol {
    var appDelegate: AppDelegate? { get set }
    var view: GetPlacesViewProtocol? { get set }
    var interactor: GetPlacesInteractor? { get set }
}

class GetPlacesPresenter: GetPlacesPresenterProtocol{
    var appDelegate: AppDelegate?
    var view: GetPlacesViewProtocol?
    var interactor: GetPlacesInteractor?
    
    init(appDelegate: AppDelegate, view: GetPlacesViewProtocol, interactor: GetPlacesInteractor) {
        self.appDelegate = appDelegate
        self.view = view
        self.interactor = interactor
    }
}

extension GetPlacesPresenter {
    //MARK: - Get Users current location
    
    func getCurrentLocation(mapView: GMSMapView, searchBar: UISearchBar){
        
        let locationManager = self.appDelegate!.locationManager
        var currentLocation : CLLocation!
        
        currentLocation = locationManager.location
    
        let lat = currentLocation.coordinate.latitude
        let long = currentLocation.coordinate.longitude

        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 10.0)
        
        mapView.camera = camera
        mapView.animate(to: camera)
        
        if searchBar.searchTextField.text != "" {
            self.searchNearByPlacesByZipCode(zipCode: searchBar.searchTextField.text!, mapView: mapView)
        }else{
            self.searchNearByPlaces(coordinates: currentLocation, mapView: mapView)
        }
    }
    
    //MARK: - Allow user to enable location services
    
    func allowUserToEnableLocationService(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)

        alertController.addAction(okAction)

        viewController.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Search location by keyword and coordinate
    
    func search(keyword: String, coordinates: CLLocation) {
        interactor?.searchInGooglePlacesAPI(coordinate: coordinates, searchCompletion: { (searchData) in
            if(searchData != nil) {
                self.appDelegate?.searchResults.removeAll()
                for item in searchData! {
                    self.appDelegate?.searchResults.append(item)
                }
            }
            self.view?.reloadRestaurantList()
        })
    }
    
    //MARK: - Search near by places
    
    func searchNearByPlaces(coordinates: CLLocation, mapView: GMSMapView) {
        interactor?.searchInGooglePlacesAPI(coordinate: coordinates, searchCompletion: { (searchData) in
            if(searchData != nil) {
                self.appDelegate?.searchResults.removeAll()
                for item in searchData! {
                    self.appDelegate?.searchResults.append(item)
                }
                if searchData?.count != 0 {
                    self.addMarkerToMap(mapView: mapView)
                }
            }
            self.view?.reloadRestaurantList()
        })
    }
    
    //MARK: - Search nearby places by zipcode
    
    func searchNearByPlacesByZipCode(zipCode: String, mapView: GMSMapView) {
        interactor?.searchRestaurantsByZipCode(zipCode: zipCode, searchCompletion: { (searchData) in
            if(searchData != nil) {
                self.appDelegate?.searchResults.removeAll()
                for item in searchData! {
                    self.appDelegate?.searchResults.append(item)
                }
                if searchData?.count != 0 {
                    self.addMarkerToMap(mapView: mapView)
                }
            }
            self.view?.reloadRestaurantList()
        })
    }
    
    //MARK: - Add marker to google map
    
    func addMarkerToMap(mapView: GMSMapView) {
        if self.appDelegate?.searchResults.count != 0 {
            mapView.clear()
            for place in self.appDelegate!.searchResults {
                guard let latitude = (place as NSDictionary).value(forKeyPath: "geometry.location.lat") as? Double else {
                    return
                }
                
                guard let longitude = (place as NSDictionary).value(forKeyPath: "geometry.location.lng") as? Double else {
                    return
                }
                
                let marker = GMSMarker()

                marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                marker.title = place["name"] as? String
                
                if let address = place["vicinity"] as? String {
                    marker.snippet = address
                }else{
                    marker.snippet = place["formatted_address"] as? String
                }
                marker.map = mapView
            }
        }
    }
    
    //MARK: - Load google Admob banner ad
    
    func loadGoogleAds(bannerView: GADBannerView, viewController: GetPlacesViewController) {
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = viewController
        bannerView.load(GADRequest())
    }
    
    //MARK: - Sign out
    
    func onSignout() {
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
        
        let sceneDelegate = UIApplication.shared.connectedScenes.first!.delegate as! SceneDelegate
        sceneDelegate.window?.rootViewController = LoginViewController()
    }
    
}


