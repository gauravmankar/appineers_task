//
//  GetPlacesInteractor.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import GooglePlaces

protocol GetPlacesInteractorProtocol {
    func searchInGooglePlacesAPI(coordinate: CLLocation, searchCompletion: @escaping (_ data: [Dictionary<String, AnyObject>]?) -> Void)
    func searchRestaurantsByZipCode(zipCode: String, searchCompletion: @escaping (_ data: [Dictionary<String, AnyObject>]?) -> Void)
}

class GetPlacesInteractor: GetPlacesInteractorProtocol {
    
    func searchInGooglePlacesAPI(coordinate: CLLocation, searchCompletion: @escaping ([Dictionary<String, AnyObject>]?) -> Void) {
        
        var placeSearchRequest = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.coordinate.latitude),\(coordinate.coordinate.longitude)&radius=5000&keyword=restaurants&key=\(Constants.GOOGLE_API_KEY)"
        placeSearchRequest = placeSearchRequest.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!

        print(placeSearchRequest)
        
        var placeApiRequest = URLRequest(url: URL(string: placeSearchRequest)!)
        placeApiRequest.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: placeApiRequest) { (data, response, error) in
            if error == nil {
                if let JSONData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers)
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        if let results = dict["results"] as? [Dictionary<String, AnyObject>] {
                            var searchResultDict: [Dictionary<String, AnyObject>] = Array()
                            for place in results {
                                searchResultDict.append(place)
                            }
                            searchCompletion(searchResultDict)
                        }
                    }
                }
            } else {
                searchCompletion(nil)
            }
        }
        
        task.resume()
    }
}

extension GetPlacesInteractor {
    
    //MARK: - Search by ZipCode
    
    func searchRestaurantsByZipCode(zipCode: String, searchCompletion: @escaping ([Dictionary<String, AnyObject>]?) -> Void) {
        var placeSearchRequest = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+\(zipCode)&key=\(Constants.GOOGLE_API_KEY)"
        placeSearchRequest = placeSearchRequest.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!

        print(placeSearchRequest)
        
        var placeApiRequest = URLRequest(url: URL(string: placeSearchRequest)!)
        placeApiRequest.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: placeApiRequest) { (data, response, error) in
            if error == nil {
                if let JSONData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers)
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        if let results = dict["results"] as? [Dictionary<String, AnyObject>] {
                            var searchResultDict: [Dictionary<String, AnyObject>] = Array()
                            for place in results {
                                searchResultDict.append(place)
                            }
                            searchCompletion(searchResultDict)
                        }
                    }
                }
            } else {
                searchCompletion(nil)
            }
        }
        
        task.resume()
    }
}
