//
//  GetPlacesRouter.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 30/09/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

protocol GetPlacesRouterProtocol {
    var rootVc: UIViewController? { get set }
}

class GetPlacesRouter: GetPlacesRouterProtocol {
    var rootVc: UIViewController?
    
    init(rootVC: UIViewController) {
        self.rootVc = rootVC
    }
}
