//
//  LoginViewController.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 01/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import FirebaseAuth
import CoreLocation

class LoginViewController: UIViewController, GIDSignInDelegate {

    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    var presenter: LoginPresenter?
    var router: LoginRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        router = LoginRouter(rootVC: self)
        presenter = LoginPresenter(router: router!)
    }
    
    //MARK: - Google Sign in
    
    @IBAction func onTapGoogleSignIn(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    //MARK: - Facebook sign in
    
    @IBAction func onTapFacebookLogin(_ sender: UIButton) {
        presenter?.loginWithFacebook(viewController: self)
    }
    
}

extension LoginViewController {
    //MARK: - Google sign in
        
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        presenter?.handleGoogleSignInRequest(signIn, didSignInFor: user, withError: error)
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
          
    }
}
