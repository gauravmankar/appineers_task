//
//  LoginPresenter.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 01/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import FirebaseAuth
import CoreLocation

protocol LoginPresenterProtocol {
    var appDelegate: AppDelegate? { get set }
    var router: LoginRouter? { get set }
}

class LoginPresenter: LoginPresenterProtocol {
    var router: LoginRouter?
    var appDelegate: AppDelegate?
    
    init(router: LoginRouter) {
        self.router = router
    }
    
    //MARK: - Login with Facebook
    
    func loginWithFacebook(viewController: UIViewController){
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: viewController) { (result, error) in
            if error == nil {
                let fbLoginResult : LoginManagerLoginResult = result!
                if fbLoginResult.grantedPermissions.contains("email") {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name"]).start { (connection, result, error) in
                if error == nil {
                    guard let result = result as? [String: Any] else {
                        return
                    }
                    
                    if let id = result["id"] as? String {
                        kUserDefaults.setValue(id, forKey: Constants.USER_ID)
                        self.router?.showGetPlacesVC()
                    }
                }
            }
        }
    }
    
    //MARK: - Login with Google
    
    func handleGoogleSignInRequest(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("error: \(error.localizedDescription)")
            return
        }
        print("User info : \(user.userID)")
            
        guard let userID = user.userID else {
            return
        }
        kUserDefaults.setValue(userID, forKey: Constants.USER_ID)
            
        self.router?.showGetPlacesVC()
        
//        let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate
//        sceneDelegate.window?.rootViewController = GetPlacesViewController()
    }
    
}
