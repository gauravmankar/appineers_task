//
//  LoginRouter.swift
//  development_tast_appineers
//
//  Created by Gaurav.Mankar on 01/10/20.
//  Copyright © 2020 Gaurav.Mankar. All rights reserved.
//

import Foundation
import UIKit

protocol LoginRouterProtocol {
    var rootVc: UIViewController? { get set }
    func showGetPlacesVC()
}

class LoginRouter: LoginRouterProtocol {
    var rootVc: UIViewController?
    
    init(rootVC: UIViewController) {
        self.rootVc = rootVC
    }
    
    func showGetPlacesVC() {
        let mapVC = GetPlacesViewController()
        mapVC.modalPresentationStyle = .fullScreen
        self.rootVc?.present(mapVC, animated: false, completion: nil)
    }
}
